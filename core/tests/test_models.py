from django.test import TestCase

from core import factories


class Tag(TestCase):

    def setUp(self):
        self.tag = factories.Tag()

    def test_str(self):
        """Тестирование строкового представления объекта"""
        self.assertEqual(
            str(self.tag),
            self.tag.name,
            'Строковое представление объекта должно браться из атрибута name'
        )

